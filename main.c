#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

const int ascii_tab = 0x09;

char *prj_dir[] = {"C","Python"};

#ifdef _WIN32
#define PATH_SEPERATOR "\\"
#else
#define PATH_SEPERATOR "/"
#endif

#define PROJ_DIR_VAR "QUICKPROJ_PROJECT_DIR"
#define EDITOR "QUICKPROJ_EDITOR"

enum lang{
    c,
    python
};

int quickprj_project_lang(char *arg){
    if(!strcmp(arg,"c")){
        return c;
    }
    else if (!strcmp(arg,"python")) {
        return python;
    }
    return -1;
}

void quickprj_project_create_dir_name(char *result_dir,const char *main_dir,char *project_dir,char *project_name){
    strcat(result_dir, main_dir);
    strcat(result_dir, PATH_SEPERATOR);
    strcat(result_dir, project_dir);
    strcat(result_dir, PATH_SEPERATOR);
    strcat(result_dir, project_name);
}

int quickprj_create_dir(char *dir){
    printf("'%s' directory creating...\n",dir);
    struct stat st = {0};
    if ( stat(dir,&st) == -1 ){
        int dir_stat = 1;
#ifdef _WIN32
        dir_stat = mkdir(dir);
#else
        dir_stat = mkdir(dir,0777);
#endif
        if(dir_stat == 0)
        {
            printf("Directory created.\n");
        }
        else
        {
            printf("Error: Directory create failed with %d: %s", dir_stat, strerror(dir_stat));
            exit(-1);
        }
        return 0;
    }
    else{
        printf("Directory already exist...\n");
        return -1;
    }
}

void quickprj_generate_makefile(char *dir,char *project_name,enum lang project_lang){
    char *source_code_filename;
    char *filename = malloc(sizeof(char) * 100);
    switch(project_lang){
        case c:
            source_code_filename = "main.c";
            printf("Generating Makefile...\n");
            strcpy(filename,dir);
            strcat(filename,"/Makefile");
            FILE *f = fopen(filename,"w");
            fprintf(f,"\
##\n\
# %s\n\
#\n\n\
CC = gcc\n\
CFLAGS = -g -Wall -Wextra -Wswitch-enum -pedantic -std=c11\n\
FILES = %s\n\
OBJ := $(FILES:.c=.o)\n\
OUTPUT = %s\n\n\n\
all: run\n\n\
%%.o:%%.c\n\
%c$(CC) -c $< -o $@ $(CFLAGS)\n\n\
compile: $(OBJ)\n\
%c$(CC) $< -o $(OUTPUT)\n\n\
run: compile\n\
%cclear\n\
%c./$(OUTPUT)\n\n\
clean:\n\
%crm $(OUTPUT)\n\
%crm $(OBJ)\n\n\n\
# end\n\
",project_name,source_code_filename,project_name,ascii_tab,ascii_tab,ascii_tab,ascii_tab,ascii_tab,ascii_tab);
            fclose(f);
            printf("Makefile generated...\n");
            break;
        case python:
            source_code_filename = "main.py";
            printf("Generating Makefile...\n");
            strcpy(filename,dir);
            strcat(filename,"/Makefile");
            FILE *file = fopen(filename,"w");
            fprintf(file,"##\n# %s\n#\n\nFILES = %s\nall: run\n\nrun:\n%cpython3 %s\n\n\n# end",project_name,source_code_filename,ascii_tab,source_code_filename);

            fclose(file);
            printf("Makefile generated...\n");
            break;
    }
    free(filename);
}

void quickprj_generate_template_code(char *dir,enum lang project_lang){
    char *filename = malloc(sizeof(char) * 100);
    printf("Generating source file...\n");
    switch(project_lang){
        case c:
            strcpy(filename,dir);
            strcat(filename,"/main.c");
            FILE *f = fopen(filename,"w");
            fprintf(f,"\
#include <stdio.h>\n\
#include <stdlib.h>\n\n\
int main(int argc,char *argv[]){\n\
%cprintf(\"Hello World!\\n\");\n\
%creturn 0;\n}\n\
",ascii_tab,ascii_tab);
            fclose(f);
            printf("Source file 'main.c' generated...\n");
            break;
        case python:
            strcpy(filename,dir);
            strcat(filename,"/main.py");
            FILE *file = fopen(filename,"w");
            fprintf(file,"def main():\n%cpass\n\n\n\n\nmain()\n",ascii_tab);
            fclose(file);
            printf("Source file 'main.py' generated...\n");
            break;
    }
    free(filename);
}

void quickprj_open_project(const char* dir)
{
    chdir(dir);
    const char* editor = getenv(EDITOR);

    if(editor == NULL || strlen(editor) == 0)
    {
        printf("WARNING: "EDITOR" not found using code!\n");
        system("code . &");
    }

    else
    {
        char cmd[256] = {};
        snprintf(cmd, sizeof(cmd), "%s . &", editor);
        system(cmd);
    }

}

int main(int argc, char *argv[]) {
    char *project_lang;
    enum lang enum_project_lang;
    char *project_name;

    const char* main_dir = getenv(PROJ_DIR_VAR);

    if(main_dir == NULL || strlen(main_dir) == 0)
    {
        printf("ERROR: Project dir not defined in env\n");
        printf("Please define "PROJ_DIR_VAR" in env\n");
        exit(-1);
    }

    if( argc < 3 ){
        printf("Error: not enough arguments\n");
        printf("Usage: quickprj <project name> <project language>\n");
        exit(-1);
    }


    project_lang = argv[2];
    enum_project_lang = quickprj_project_lang(project_lang);
    if (enum_project_lang == -1) {
        printf("Unknown Language\n");
        exit(-1);
    }

    char dir[256] = {};
    project_name = argv[1];

    quickprj_project_create_dir_name(dir,main_dir,prj_dir[enum_project_lang],project_name);
    int dir_status = quickprj_create_dir(dir);

    if ( dir_status == -1 ){
        printf("This process overwrite your files are you sure(y/n):");
        char user_input;
        scanf("%c",&user_input);    

        if ('n' == user_input || 'N' == user_input) {
            quickprj_open_project(dir);
            printf("Program Complete...\n");
            exit(0);
        }

        else if('y' == user_input || 'Y' == user_input) {
            printf("Process continued.\n");
        }

        else{
            printf("Wrong input.Exiting...\n");
            exit(-1);
        }

    }
    quickprj_generate_makefile(dir,project_name,enum_project_lang);
    quickprj_generate_template_code(dir,enum_project_lang);
    printf("Switching directory to '%s'\n",dir);
    printf("Project opening in code...\n");
    quickprj_open_project(dir);
    printf("Program Complete...\n");
    return 0;
}

