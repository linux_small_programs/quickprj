##
# quickprj
#
# @file
# @version 0.1

CC = gcc
FLAGS = -O2 -Wall
FILES = main.c
OUTPUT = quickprj
ARGS = deneme c

all: compile

compile:
	$(CC) $(FLAGS) $(FILES) -o $(OUTPUT)

run: compile
	./$(OUTPUT) $(ARGS)

clean:
	rm $(OUTPUT)

install:
	ln -s $(shell pwd)/$(OUTPUT) /usr/bin/$(OUTPUT)
# end
